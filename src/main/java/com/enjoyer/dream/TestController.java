package com.enjoyer.dream;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello-world")
public class TestController {

    @RequestMapping("test")
    public String test() {
        return "测试";
    }
}
